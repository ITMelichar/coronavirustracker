package cz.derdak.coronavirustracker

data class Model(
    val id: Int,
    val country: String,
    val totalcases: String,
    val newcases: String,
    val totaldeaths: String,
    val newdeaths: String,
    val totalrecovered: String,
    val activecases: String,
    val critical: String
    )


/*
class Model {

    val country: String? = null
    val totalcases: String? = null
    val newcases: String? = null
    val totaldeaths: String? = null
    val newdeaths: String? = null
    val activecases: String? = null
    val totalrecovered: String? = null
    val critical: String? = null
    val totalglobalcases: String? = null
    val totalglobaldeaths: String? = null
    val totalglobalrecovered: String? = null
    val totalglobalactive: String? = null

    fun getCountrys(): String {
        return country.toString()
    }

    fun getTotalGlobalCases(): String {
        return totalglobalcases.toString()
    }


    fun getTotalGlobalDeaths(): String {
        return totalglobaldeaths.toString()
    }

    fun getTotalGlobalRecovered(): String {
        return totalglobalrecovered.toString()
    }

    fun getTotalGlobalActive(): String {
        return totalglobalactive.toString()
    }


    fun getTotalCases(): String {
        return totalcases.toString()
    }

    fun getNewCases(): String {
        return newcases.toString()
    }

    fun getTotalDeaths(): String {
        return totaldeaths.toString()
    }

    fun getNewDeaths(): String {
        return newdeaths.toString()
    }

    fun getActiveCases(): String {
        return activecases.toString()
    }

    fun getTotalRecovereds(): String {
        return totalrecovered.toString()
    }

    fun getCriticals(): String {
        return critical.toString()
    }

}

class Data {
    val status:String? = null
    val version:String? = null
    val message:String? = null
    val data:List<Model>? = null
}
*/