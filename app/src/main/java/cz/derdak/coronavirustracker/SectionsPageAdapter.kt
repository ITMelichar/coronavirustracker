package cz.derdak.coronavirustracker

import android.content.Context
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentPagerAdapter


class SectionsPageAdapter(private val _Context: Context, fm: FragmentManager, private var totalTabs: Int) : FragmentPagerAdapter(fm) {
    // this is for fragment tabs
    override fun getItem(position: Int): Fragment {
        when (position) {
            0 -> {
                return TotalFragment()
            }
            1 -> {
                return DetailFragment()
            }
            /*2 -> {
                return MapFragment()
            }*/

        }
        return TotalFragment()
    }

    // this counts total number of tabs
    override fun getCount(): Int {
        return totalTabs
    }

    override fun getPageTitle(position: Int): CharSequence? {
        return when (position) {
            0 -> _Context.resources.getString(R.string.total)
            1 -> _Context.resources.getString(R.string.detail)
            //2 -> _Context.resources.getString(R.string.map)
            else -> null
        }
    }

}