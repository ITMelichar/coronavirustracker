package cz.derdak.coronavirustracker

import android.app.ProgressDialog
import android.os.Bundle
import android.widget.Toast
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import androidx.viewpager.widget.ViewPager
import com.google.android.material.tabs.TabLayout
import java.util.*


class MainActivity : AppCompatActivity() {

    private var progressDialog: ProgressDialog? = null
    //private var swipeRefreshLayout: SwipeRefreshLayout? = null

    private var tabLayout: TabLayout? = null
    private var viewPager: ViewPager? = null


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

//TODO Refresh Layout and SearchBar
            tabLayout = findViewById(R.id.tabLayout)
            viewPager = findViewById(R.id.viewPager)

            tabLayout!!.addTab(tabLayout!!.newTab().setText(getString(R.string.total)))
            tabLayout!!.addTab(tabLayout!!.newTab().setText(getString(R.string.detail)))
            //tabLayout!!.addTab(tabLayout!!.newTab().setText(getString(R.string.map)))
            tabLayout!!.tabGravity = TabLayout.GRAVITY_FILL

            val adapter = SectionsPageAdapter(this, supportFragmentManager, tabLayout!!.tabCount)
            viewPager!!.adapter = adapter

            tabLayout!!.setupWithViewPager(viewPager)


            progressDialog = ProgressDialog(this)
            progressDialog!!.setCancelable(false)
            progressDialog!!.setMessage(getString(R.string.loading))
            progressDialog!!.setProgressStyle(ProgressDialog.STYLE_SPINNER)
            progressDialog!!.progress = 0
            progressDialog!!.show()

            val t = Timer()
            t.schedule(object : TimerTask() {
                override fun run() {
                    progressDialog!!.dismiss() // when the task active then close the dialog
                    t.cancel() // also just top the timer thread, otherwise, you may receive a crash report
                }
            }, 2000) // after 2 second (or 2000 miliseconds), the task will be active.


        //}

       /* swipeRefreshLayout = findViewById(R.id.swipe) as SwipeRefreshLayout

        swipeRefreshLayout!!.setOnRefreshListener {
            swipeRefreshLayout!!.isRefreshing = false
            //your code on swipe refresh
            //we are checking networking connectivity
        }*/





        //REFERENCE MATERIALSEARCHBAR AND LISTVIEW
      /* val lv = findViewById(R.id.lv) as ListView
        val searchBar = findViewById(R.id.searchbar) as MaterialSearchBar
        searchBar.setHint("Search..")
        searchBar.setSpeechMode(true)


        //ADAPTER
        val adapter = ArrayAdapter<String>(this, android.R.layout.simple_list_item_1, galaxies)
        lv.setAdapter(adapter)

        //SEARCHBAR TEXT CHANGE LISTENER
        searchBar.addTextChangeListener(object : TextWatcher {
            override fun beforeTextChanged(charSequence: CharSequence, i: Int, i1: Int, i2: Int) {

            }

            override fun onTextChanged(charSequence: CharSequence, i: Int, i1: Int, i2: Int) {
                //SEARCH FILTER
                adapter.getFilter().filter(charSequence)
            }

            override fun afterTextChanged(editable: Editable) {

            }
        })

        //LISTVIEW ITEM CLICKED
        lv.setOnItemClickListener(object : AdapterView.OnItemClickListener {
            override fun onItemClick(adapterView: AdapterView<*>, view: View, i: Int, l: Long) {
                Toast.makeText(this@MainActivity, adapter.getItem(i)!!.toString(), Toast.LENGTH_SHORT).show()
            }
        })
*/

       /* var countries = modelArrayList?.size?.let { arrayOfNulls<String>(it) }
        modelArrayList?.toArray(countries)

        println("Countries: " + Arrays.toString(countries))*/

    }
}