package cz.derdak.coronavirustracker

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ListView
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.LinearLayoutManager
import com.github.kittinunf.fuel.Fuel
import com.github.kittinunf.fuel.android.extension.responseJson
import kotlinx.android.synthetic.main.fragment_detail.*
import kotlinx.android.synthetic.main.fragment_total.*
import org.json.JSONException
import org.json.JSONObject
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.util.*

class DetailFragment : Fragment() {

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,savedInstanceState: Bundle?): View {

        val detailView: View =
            inflater.inflate(R.layout.fragment_detail, container, false)

        DataApi().getAllData().enqueue(object : Callback<List<Model>> {
            override fun onFailure(call: Call<List<Model>>, t: Throwable) {
                Toast.makeText(context, t.message, Toast.LENGTH_LONG).show()
                print(t.message)
            }

            override fun onResponse(call: Call<List<Model>>, response: Response<List<Model>>) {

                val data = response.body()

                data?.let {
                    showData(it)
                }

            }

        })

        return detailView
    }

private fun showData(detailData: List<Model>) {

    recyclerView_detail.layoutManager = LinearLayoutManager(context)
    recyclerView_detail.adapter = DetailRecyclerAdapter(detailData)

}
}