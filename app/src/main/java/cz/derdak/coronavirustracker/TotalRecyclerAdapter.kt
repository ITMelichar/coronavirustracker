package cz.derdak.coronavirustracker

import android.annotation.SuppressLint
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import kotlinx.android.synthetic.main.lv_total_item.view.*

class TotalRecyclerAdapter(private val totalData : List<Model>) : RecyclerView.Adapter<TotalRecyclerAdapter.ModelViewHolder>(){

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ModelViewHolder {
        return ModelViewHolder(
            LayoutInflater.from(parent.context)
                .inflate(R.layout.lv_total_item, parent, false)
        )
    }

    override fun getItemCount() = totalData.size

    @SuppressLint("SetTextI18n")
    override fun onBindViewHolder(holder: TotalRecyclerAdapter.ModelViewHolder, position: Int) {
        val item = totalData[position]

        holder.view.totalglobaldeaths.text = item.totaldeaths
        holder.view.totalglobalrecovered.text = item.totalrecovered
        holder.view.totalglobalactive.text = item.activecases + holder.view.context.resources.getString(R.string.slash) + item.totalcases
        holder.view.totalglobalcritical.text = item.critical

    }

    inner class ModelViewHolder(val view: View) : RecyclerView.ViewHolder(view){

    }
}