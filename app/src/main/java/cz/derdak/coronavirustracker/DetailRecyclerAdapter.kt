package cz.derdak.coronavirustracker

import android.annotation.SuppressLint
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import kotlinx.android.synthetic.main.lv_detail_item.view.*

class DetailRecyclerAdapter(private val detailData : List<Model>) : RecyclerView.Adapter<DetailRecyclerAdapter.ModelViewHolder>() {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ModelViewHolder {
        return ModelViewHolder(
            LayoutInflater.from(parent.context)
                .inflate(R.layout.lv_detail_item, parent, false)
        )
    }

    override fun getItemCount() = detailData.size


    @SuppressLint("SetTextI18n")
    override fun onBindViewHolder(holder: DetailRecyclerAdapter.ModelViewHolder, position: Int) {
        val item = detailData[position]

        val path = holder.view.context.resources

        holder.view.country.text = item.country
        holder.view.totalcases.text = path.getString(R.string.totalcases) + item.totalcases
        holder.view.newcases.text = path.getString(R.string.newcases) + item.newcases
        holder.view.totaldeaths.text = path.getString(R.string.totaldeaths) + item.totaldeaths
        holder.view.newdeaths.text = path.getString(R.string.newdeaths) + item.newdeaths
        holder.view.activecases.text = path.getString(R.string.activecases) + item.activecases
        holder.view.totalrecovered.text = path.getString(R.string.totalrecovered) + item.totalrecovered
        holder.view.critical.text = path.getString(R.string.critical) + item.critical

    }


    inner class ModelViewHolder(val view: View) : RecyclerView.ViewHolder(view){

    }
}