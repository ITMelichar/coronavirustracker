package cz.derdak.coronavirustracker

import android.app.DownloadManager
import android.content.Context
import android.content.Intent
import android.graphics.Color
import android.net.ConnectivityManager
import android.net.NetworkInfo
import android.net.Uri
import android.os.Build
import android.os.Bundle
import android.os.Handler
import android.os.StrictMode
import android.widget.ImageView
import android.widget.TextView
import android.widget.Toast
import androidx.annotation.RequiresApi
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.view.ContextThemeWrapper
import java.net.URL
import java.util.*


class SplashScreen : AppCompatActivity() {

    //Value of waiting for redirect (1000 = 1s)
    private val time = 1500
    private val url = URL("https://itmelichar.cz/assets/apk/apk.version")
    private var versionCode: Int? = null
    private var downloaded: Boolean? = null

    //Function, which start onCreate activity
    @RequiresApi(Build.VERSION_CODES.KITKAT)
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_splash_screen)

        val imageView = findViewById<ImageView>(R.id.imageView)
        val text = findViewById<TextView>(R.id.APIsource)

        val policy =
            StrictMode.ThreadPolicy.Builder().permitAll().build()
        StrictMode.setThreadPolicy(policy)

        versionCode = BuildConfig.VERSION_CODE

        val connectivityManager = applicationContext.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
        val activeNetwork: NetworkInfo? = connectivityManager.activeNetworkInfo
        val isConnected: Boolean = activeNetwork?.isConnectedOrConnecting == true

        imageView.setImageResource(R.drawable.covid)

        if(!isConnected) {
            text.textSize = 26F
            text.setTextColor(Color.RED)
            text.text = resources.getString(R.string.connectionError)
            Toast.makeText(
                applicationContext,
                resources.getString(R.string.connectionError),
                Toast.LENGTH_LONG
            ).show()
        } else if(isConnected && (url.readText().trim() > versionCode.toString().trim())) {

            println((versionCode.toString()).trim())
            println((url.readText()).trim())

            val builderX = AlertDialog.Builder(ContextThemeWrapper(this, R.style.myDialog))
            builderX.setTitle(getString(R.string.updateTitle))
            builderX.setMessage(getString(R.string.updateMessage))
            builderX.setPositiveButton(getString(R.string.updateYes)){dialog, which ->

                val openURL = Intent(Intent.ACTION_VIEW)
                openURL.data = Uri.parse("https://itmelichar.cz/assets/apk/covid19tracker.apk")
                finish()
                startActivity(openURL)
                //downloadFile()
            }
            builderX.setNeutralButton(getString(R.string.updateNo)){_,_ ->
                handler()
            }
            val dialog: AlertDialog = builderX.create()
            dialog.show()

        } else {

            handler()
        }
    }

    private fun handler() {
        //After time -> Body of handler will start
        Handler().postDelayed({

            //redirect to MainActivity
            val intent = Intent(this@SplashScreen, MainActivity::class.java)
            startActivity(intent)
            finish()
        }, time.toLong())
    }

    /* @RequiresApi(Build.VERSION_CODES.KITKAT)
     fun downloadFile(downloaded: Boolean) : Boolean {
          val DownloadUrl: String = "https://itmelichar.cz/assets/apk/covid19tracker.apk"
          val request1 = DownloadManager.Request(Uri.parse(DownloadUrl))
          request1.setDescription("Coronavirus Tracker app") //appears the same in Notification bar while downloading
          request1.setTitle("update.apk")
          request1.setVisibleInDownloadsUi(false)
          if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
              request1.allowScanningByMediaScanner()
              request1.setNotificationVisibility(DownloadManager.Request.VISIBILITY_HIDDEN)
          }
          request1.setDestinationInExternalFilesDir(applicationContext, "/File", "update.apk")
          val manager1 =
              getSystemService(Context.DOWNLOAD_SERVICE) as DownloadManager
          Objects.requireNonNull(manager1).enqueue(request1)
          if (DownloadManager.STATUS_SUCCESSFUL == 8) {
              Toast.makeText(applicationContext, "Successful", Toast.LENGTH_SHORT).show()

              val promptInstall = Intent(Intent.ACTION_VIEW)
                  .setDataAndType(
                      Uri.parse("content:///data/user/0/cz.derdak.coronavirustracker/files/File/update.apk"),
                      "application/vnd.android.package-archive"
                  )
              finish()
              startActivity(promptInstall)

          }
         return true
      }*/

}
