package cz.derdak.coronavirustracker

import com.google.gson.GsonBuilder
import retrofit2.Call
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.http.GET


const val BASE_URL = "https://api.cov2019.info/v5/data/today/"

interface DataApi {

    @GET("summary-only-data")
    fun getTotalData() : Call<List<Model>>

    @GET("all-only-data")
    fun getAllData(): Call<List<Model>>


    companion object {
        operator fun invoke() : DataApi {
            val gson = GsonBuilder()
                .setLenient()
                .create()
            return Retrofit.Builder()
                .baseUrl(BASE_URL)
                .addConverterFactory(GsonConverterFactory.create(gson))
                .build()
                .create(DataApi::class.java)
        }
    }

}