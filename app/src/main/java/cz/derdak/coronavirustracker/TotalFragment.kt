package cz.derdak.coronavirustracker

import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.text.format.DateFormat.format
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.*
import androidx.appcompat.app.AlertDialog
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.LinearLayoutManager
import com.github.kittinunf.fuel.Fuel
import com.github.kittinunf.fuel.android.extension.responseJson
import kotlinx.android.synthetic.main.fragment_total.*
import org.json.JSONException
import org.json.JSONObject
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.text.DateFormat
import java.util.*


class TotalFragment : Fragment() {

    private var problemReportButton: Button? = null

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {

        val totalView: View =
            inflater.inflate(R.layout.fragment_total, container, false)

        DataApi().getTotalData().enqueue(object : Callback<List<Model>> {
            override fun onFailure(call: Call<List<Model>>, t: Throwable) {
                Toast.makeText(context, t.message, Toast.LENGTH_LONG).show()
                print(t.message)
            }

            override fun onResponse(call: Call<List<Model>>, response: Response<List<Model>>) {

                val data = response.body()

                data?.let {
                    showData(it)
                }

            }

        })

        problemReportButton = totalView.findViewById(R.id.problemReportBtn) as Button

        problemReportButton!!.setOnClickListener {
            val builder = AlertDialog.Builder(context!!)
            builder.setTitle(getString(R.string.problemReport))

            val input = EditText(context)
            val lp = LinearLayout.LayoutParams(
                LinearLayout.LayoutParams.MATCH_PARENT,
                LinearLayout.LayoutParams.MATCH_PARENT
            )

            input.layoutParams = lp
            lp.setMargins(16, 2, 8, 16)
            builder.setView(input)

            // Display a message on alert dialog
            builder.setMessage(getString(R.string.problemMessage))

            // Set a positive button and its click listener on alert dialog
            builder.setPositiveButton(getString(R.string.problemMessageSend)) { dialog, which ->

                sendEmail(
                    "it.janmelichar@gmail.com",
                    "Covid Tracker issue",
                    (input.text).toString()
                )
                Toast.makeText(context, "Thank you!", Toast.LENGTH_SHORT).show()

            }
            // Display a neutral button on alert dialog
            builder.setNeutralButton(getString(R.string.problemMessageCancel)) { _, _ ->
            }


            // Finally, make the alert dialog using builder
            val dialog: AlertDialog = builder.create()

            // Display the alert dialog on app interface
            dialog.show()
        }
        return totalView
    }

    private fun sendEmail(recipient: String, subject: String, message: String) {
        /*ACTION_SEND action to launch an email client installed on your Android device.*/
        val mIntent = Intent(Intent.ACTION_SEND)
        /*To send an email you need to specify mailto: as URI using setData() method
        and data type will be to text/plain using setType() method*/
        mIntent.data = Uri.parse("mailto:")
        mIntent.type = "text/plain"
        // put recipient email in intent
        /* recipient is put as array because you may wanna send email to multiple emails
           so enter comma(,) separated emails, it will be stored in array*/
        mIntent.putExtra(Intent.EXTRA_EMAIL, arrayOf(recipient))
        //put the Subject in the intent
        mIntent.putExtra(Intent.EXTRA_SUBJECT, subject)
        //put the message in the intent
        mIntent.putExtra(Intent.EXTRA_TEXT, message)


        try {
            //start email intent
            startActivity(Intent.createChooser(mIntent, getString(R.string.problemMessageEmail)))
        }
        catch (e: Exception){
            //if any thing goes wrong for example no email client application or any exception
            //get and show exception message
            Toast.makeText(context, e.message, Toast.LENGTH_LONG).show()
        }

    }
    private fun showData(totalData: List<Model>) {

        recyclerView_total.layoutManager = LinearLayoutManager(context)
        recyclerView_total.adapter = TotalRecyclerAdapter(totalData)

    }
}